import discord
class BroodBot(discord.Client):

    def __init__(self, my_guild : discord.Object):
        self.my_guild = my_guild
        super().__init__(intents=discord.Intents.default())
        self.tree = discord.app_commands.CommandTree(self)
    
    async def setup_hook(self):
        self.tree.copy_global_to(guild=self.my_guild)
        await self.tree.sync(guild=self.my_guild)

class ProductChoiceView(discord.ui.View):
    
    async def select_callback(self, select, interaction): # the function called when the user is done selecting options
        select.disabled = True
        await interaction.response.edit_message(f"Awesome! I like {select.values[0]} too!")