import discord
import logging
from discord import app_commands
import os
from dotenv import load_dotenv
from datetime import date

import broodbode
import broodbot
load_dotenv()

logger = logging.FileHandler(filename='broodbode.log', encoding='utf-8', mode='w')

BROODBODE_URI = os.environ.get('BROODBODE_URI').replace('TODAY', date.today().strftime('fmt'))

DISCORD_TOKEN = os.environ.get('DISCORD_TOKEN')
DISCORD_GUILD = os.environ.get('DISCORD_GUILD')
DISCORD_CHANNEL = os.environ.get('DISCORD_CHANNEL')
DISCORD_ADMIN = os.environ.get('DISCORD_ADMIN')

broodbode = broodbode.BroodBode(BROODBODE_URI)


discord_client = broodbot.BroodBot(discord.Object(DISCORD_GUILD))

@discord_client.tree.command()
async def make_a_choice(interaction: discord.Interaction):
    product_choice_view = broodbot.ProductChoiceView()

    #Add the spreads
    spread_select = discord.ui.Select(placeholder='Kies een product')
    for index, spread in enumerate(broodbode.available_spreads):
        spread_select.add_option(
            label=str(spread)[:80],
            value=spread.id,
            description=spread.description[:80]
        )
        if index == 24:
            break
    
    product_choice_view.add_item(spread_select)
    await interaction.response.send_message(content='Kies een broodje..', view=product_choice_view)

discord_client.run(token=DISCORD_TOKEN)