import requests, json
from discord import Member


class BreadType:
    '''A breadtype, grain, whole-grain, spelt, Gluten-free'''
    def __init__(self, name : str, surcharge : float, active : bool = True):
        self.name = name
        self.surcharge = surcharge
        self.active = active

class Spread:
    '''Spread on the breadtype, for example: Goatechees, Cheese, Capra, Muhammara''' 
    def __init__(self, id : int, name : str, description : str, price : float, breadtypes : []):
        self.id = id
        self.name = name
        self.description = description
        self.price = price
        self.available_breadtypes = breadtypes

    def __str__(self):
        return f"{self.name} - \N{euro sign} {self.price}"

class Product:
    '''User combination breadtype and spreak'''
    def __init__(self, spread: Spread, breadtype: BreadType):
        self.spread = spread
        self.type = breadtype

    def price(self):
        return self.spread.price + self.type.surcharge

    def __str__(self):
        return f"{self.spread.name} on a {self.type.name} bun."


class Order:
    '''The product that is orderd from a user'''
    def __init__(self, product : Product, user : Member):
        self.product = product
        self.user = user

    def __str__(self):
        return f"{self.user.display_name} - {self.product} for \N{euro sign} {self.product.price()}"

class BroodBode:
    orders = []
    available_spreads = []
    available_breadtypes = []
    def __init__(self, uri = str, orders : [Order] = []):
        self.orders = orders
        self.uri = uri
        self.__init_products()

    def __init_products(self):
        result = requests.api.get(self.uri).json()

        #Retrieve all breadtypes
        for breadtype in result['breadtypes']:
            self.available_breadtypes.append(
                BreadType( name=breadtype['name'], surcharge=float(breadtype['surcharge']), active=bool(breadtype['active']))
            )

        #Retrieve all spreads (Categorie = 1)
        for product in result['products']:
            if product['categorie_id'] == 1:
                self.available_spreads.append(
                    Spread( \
                        id=product['id'], \
                        name=product['title'], \
                        description=product['subtitle'], \
                        price=float(product['price']), \
                        breadtypes=json.loads(product['breadtypes'])\
                    )
                )